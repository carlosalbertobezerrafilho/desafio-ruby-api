1. Instalando o Console do Cmder
	Baixe em: https://github.com/cmderdev/cmder/releases/
	Descompactar na pasta C:\tools\Cmder.
	Selecione o cmder.exe e arraste até sua barra de ferramentas do Windows para criar um atalho.
	Executar o cmder.exe.

2. Instalando o Ruby para Windows
	Baixe em: http://rubyinstaller.org/downloads/.
	Executar o arquivo baixado e seguir as instruções clicando em 'next‘.
	Selecionar os 3 checkbox exibidos e continuar a dar 'next' até o 'finish'.
	No Console do Cmder, digite o comando `ruby –v`, caso tenha sido instaldo corretamente a versão instalada é exibida.

3. Instalando Devkit
	Baixe em: http://dl.bintray.com/oneclick/rubyinstaller/DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe.  
	Acesse o diretório C:\Ruby23-x64.
	Crie uma pasta chamada `devKit` e coloque o arquivo baixado dentro dessa pasta.
	Clique duas vezes no arquivo para que ele descompacte os arquivos na pasta que foi criada.
	No Console do Cmder, digite os comandos:
   	 > cd C:\Ruby23-x64\devkit
   	 > ruby dk.rb init
   	 > ruby dk.rb install

4. Instalando o bundler
	No Console do Cmder, digite o comando:
 	 > gem install bundler

5. Gems utilizadas
	No arquivo Gemfile, adicione as seguintes gems utilizadas e no console do Cmder utilize o comando mostrado anteriormente.
		gem 'cucumber'
		gem 'httparty'
		gem 'ffi'
		gem 'faker'

