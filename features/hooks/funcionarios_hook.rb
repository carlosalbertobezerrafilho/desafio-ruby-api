Before '@teste' do

    $nome    = Faker::Name.name
    $idade   = Faker::Number.between(from: 18, to: 60)
    $salario = "1000"

    body = {
        "name": $nome,
        "salary": $salario,
        "age": $idade
    }

    @body        = JSON.generate(body)
    @funcionario = Funcionarios.new(@body)

end