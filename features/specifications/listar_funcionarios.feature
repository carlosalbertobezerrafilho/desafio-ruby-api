#language: pt
#utf-8
@teste
Funcionalidade: Listar Funcionarios
Como um usuário do sistema
Eu quero realizar as requisições na API
A fim de listar as informações dos funcionários

	@listar_funcionarios
    Cenário: Listar todos os empregados com salário maior que R$10.000,00 
     Dado que efetuo a requisicao GET para o servico
     Quando filtrar a lista com os funcionarios de salario acima de R$10.000,00
     Então o servico deve exibir lista dos funcionarios com salario maior que R$10.000,00