#language: pt
#utf-8
@teste
Funcionalidade: Cadastrar e Atualizar Dados de Funcionarios
Como um usuário do sistema
Eu quero realizar as requisições na API
A fim de criar e manipular as informações do cadastro de funcionários

	@criar_e_atualizar_funcionario
    Cenário: Cadastrar e Atualizar Dados de Funcionarios
     Dado que o usuario acesse o servico para criar um novo funcionario
     Quando criar um novo funcionario com os dados gerados
     E o servico retornar os dados do funcionario criado
     Quando buscar esse novo funcionario atraves do ID gerado no passo anterior
     E alterar o salario para R$10.000,00
     Então o servico deve retornar os dados atualizados do funcionario
 
     



