class Funcionarios
    include HTTParty
    require 'json'
    require_relative '../hooks/funcionarios_hook'

    base_uri       "http://dummy.restapiexample.com/api/v1"
    $post_url    = "/create"
    $get_url     = "/employee/"
    $get_all_url = "/employees"
    $put_id_url  = "/update/"
    
    $id = "";

    def initialize(body)
        @options =  {:body => body}
        
    end

    def postFuncionario
       $response     = self.class.post($post_url, @options)
       $postBodyJson = JSON.parse($response.body)
       $id           = $postBodyJson["id"]
    end

    def getFuncionarioID()
        $responseGet = self.class.get($get_url + $id)
        $getBodyJson = JSON.parse($responseGet.body)
        validarFuncionario($getBodyJson)
    end

    def validarFuncionario(bodyFuncionario)
        bodyFuncionario["id"].eql?($id)
        bodyFuncionario["employee_name"].eql?($nome)
        bodyFuncionario["employee_salary"].eql?($salario)
        bodyFuncionario["employee_age"].eql?($idade)
    end

    def putFuncionarioSalarioAtualizado
        $responsePutAtualizado = self.class.put($put_id_url+$id, :body => { 
                                            'name'  => $nome,
                                            'salary'=> 10000,
                                            'age'   => $idade,
                                            'id'    => $id
                                            }.to_json)
        $putSalarioAtualizadoBodyJson = JSON.parse($responsePutAtualizado.body)
        validarFuncionarioSalarioAtualizado($putSalarioAtualizadoBodyJson)
    end

    def validarFuncionarioSalarioAtualizado(bodyFuncionarioSalarioAtualizado)
        bodyFuncionarioSalarioAtualizado["id"].eql?($id)
        bodyFuncionarioSalarioAtualizado["employee_name"].eql?($nome)
        bodyFuncionarioSalarioAtualizado["employee_salary"].eql?("10000")
        bodyFuncionarioSalarioAtualizado["employee_age"].eql?($idade)
    end

    def getFuncionariosSalarioDezMil
        $getTodosFuncionarios = self.class.get($get_all_url)
        $bodyGetTodosFuncionarios = JSON.parse($getTodosFuncionarios.body)
    end

    def filtrarFuncionariosSalarioDezMil
        
    end

    def validarFuncionarioSalarioDezMil
        $bodyGetTodosFuncionarios.each do |funcionarios|
        $salarioFuncionario = "#{funcionarios['employee_salary']}"
            if ($salarioFuncionario.to_i >= 10000)
                puts "Nome do Funcionario: #{funcionarios['employee_name']}, Salario: #{funcionarios['employee_salary']}, ID: #{funcionarios['id']}"
            end
        end
    end

   
end