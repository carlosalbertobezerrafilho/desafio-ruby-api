Dado("que o usuario acesse o servico para criar um novo funcionario") do
    $base_uri = HTTParty.get("http://dummy.restapiexample.com/create") 
    expect($base_uri.response.code).to eql "200" 
end

Quando("criar um novo funcionario com os dados gerados") do 
    @funcionario.postFuncionario
end

E ("o servico retornar os dados do funcionario criado") do
    puts "Response Body #{$response.body}"
end

Quando ("buscar esse novo funcionario atraves do ID gerado no passo anterior") do
    @funcionario.getFuncionarioID
end

E ("alterar o salario para R$10.000,00") do
    @funcionario.putFuncionarioSalarioAtualizado
end

Então ("o servico deve retornar os dados atualizados do funcionario") do
    puts "Response Body #{$putSalarioAtualizadoBodyJson}"
end

