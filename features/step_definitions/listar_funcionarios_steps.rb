Dado("que efetuo a requisicao GET para o servico") do
    @funcionario.getFuncionariosSalarioDezMil  
end
  
Quando("filtrar a lista com os funcionarios de salario acima de R$10.000,00") do
    @funcionario.filtrarFuncionariosSalarioDezMil
end
  
Então("o servico deve exibir lista dos funcionarios com salario maior que R$10.000,00") do
    @funcionario.validarFuncionarioSalarioDezMil
end